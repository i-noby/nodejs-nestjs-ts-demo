import { ApiProperty } from '@nestjs/swagger'
import { ObjectId } from 'mongodb'
import {
  Infer,
  RequiredObjectSchema,
  RequiredStringSchema,
} from '../../schema/base.schema'

/**
 * 老師
 */
export class Teacher {
  /**
   * _id
   * @example 64bc39fb5e73eeebbcda7d54
   */
  @ApiProperty({
    description: '_id ',
    type: ObjectId,
    required: true,
    example: '64bc39fb5e73eeebbcda7d54',
  })
  _id!: ObjectId
  /**
   * 姓名
   * @example 王小明
   */
  @ApiProperty({
    description: '姓名 ',
    type: String,
    required: true,
    example: '王小明',
  })
  name!: String
}

export const TeacherSchema = () =>
  RequiredObjectSchema({
    name: RequiredStringSchema(),
  })

export type TeacherDto = Infer<ReturnType<typeof TeacherSchema>> | Teacher
