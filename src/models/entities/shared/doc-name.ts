export enum DocName {
  Parents = 'Parents',
  Teachers = 'Teachers',
  Students = 'Students',
}
