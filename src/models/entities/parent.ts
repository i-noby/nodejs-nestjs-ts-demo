import { ApiProperty } from '@nestjs/swagger'
import { ObjectId } from 'mongodb'
import {
  Infer,
  RequiredObjectSchema,
  RequiredStringSchema,
} from '../../schema/base.schema'

/**
 * 家長
 */
export class Parent {
  /**
   * _id
   * @example 64bc39fb5e73eeebbcda7d54
   */
  @ApiProperty({
    description: '_id ',
    type: ObjectId,
    required: true,
    example: '64bc39fb5e73eeebbcda7d54',
  })
  _id!: ObjectId
  /**
   * 姓名
   * @example 王小明
   */
  @ApiProperty({
    description: '姓名 ',
    type: String,
    required: true,
    example: '王小明',
  })
  name!: String
}

export const ParentSchema = () =>
  RequiredObjectSchema({
    name: RequiredStringSchema(),
  })

export type ParentDto = Infer<ReturnType<typeof ParentSchema>> | Parent
