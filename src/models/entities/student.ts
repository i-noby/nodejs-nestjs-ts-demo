import { ApiProperty } from '@nestjs/swagger'
import { ObjectId } from 'mongodb'
import {
  Infer,
  NullableBooleanSchema,
  NullableDateSchema,
  NullableNumberSchema,
  RequiredObjectSchema,
  RequiredStringSchema,
} from '../../schema/base.schema'

/**
 * 學生
 */
export class Student {
  /**
   * _id
   * @example 64bc39fb5e73eeebbcda7d54
   */
  @ApiProperty({
    description: '_id ',
    type: ObjectId,
    required: true,
    example: '64bc39fb5e73eeebbcda7d54',
  })
  _id!: ObjectId
  /**
   * 姓名
   * @example 王小明
   */
  @ApiProperty({
    description: '姓名 ',
    type: String,
    required: true,
    example: '王小明',
  })
  name!: String
  /**
   * 班級
   * @example 三年二班
   */
  @ApiProperty({
    description: '班級 ',
    type: String,
    required: true,
    example: '三年二班',
  })
  className!: String
  /**
   * 座號
   * @example 49
   */
  @ApiProperty({
    description: '座號 ',
    type: Number,
    required: false,
    example: '49',
  })
  seatNumber?: Number
  /**
   * 生日
   * @example 2020-02-02
   */
  @ApiProperty({
    description: '生日 ',
    type: Date,
    required: false,
    example: '2020-02-02',
  })
  birthday?: Date
  /**
   * 是否啟用
   * @description 停用則禁止選取
   * @example true
   */
  @ApiProperty({
    description: '是否啟用 停用則禁止選取',
    type: Boolean,
    required: false,
    example: 'true',
  })
  enabled?: Boolean
}

export const StudentSchema = () =>
  RequiredObjectSchema({
    name: RequiredStringSchema(),
    className: RequiredStringSchema(),
    seatNumber: NullableNumberSchema(),
    birthday: NullableDateSchema(),
    enabled: NullableBooleanSchema(),
  })

export type StudentDto = Infer<ReturnType<typeof StudentSchema>> | Student
