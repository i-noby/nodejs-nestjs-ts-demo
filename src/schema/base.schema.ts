import dayjs from 'dayjs'
import { ObjectId } from 'mongodb'
import { ZodObject, ZodRawShape, ZodSchema, ZodType, z } from 'zod'
import { isNullOrEmpty } from '../helpers/util.helper'

function nullableAndToNull<T extends ZodSchema>(schema: T) {
  return schema
    .nullish()
    .optional()
    .transform((value) => (isNullOrEmpty(value) ? null : value))
}

function nullableAndToEmpty<T extends ZodSchema>(schema: T) {
  return schema
    .nullish()
    .transform((value) => (isNullOrEmpty(value) ? '' : value))
}

export const RequiredStringSchema = () => z.string().nonempty()

export const NullableStringSchema = () => nullableAndToEmpty(z.string())

export const RequiredEmailSchema = () => RequiredStringSchema().email()

export const NullableEmailSchema = () =>
  nullableAndToEmpty(RequiredEmailSchema())

export const RequiredUrlSchema = () => RequiredStringSchema().url()

export const NullableUrlSchema = () => nullableAndToEmpty(RequiredUrlSchema())

export const RequiredNumberSchema = () => z.number()

export const NullableNumberSchema = () =>
  nullableAndToNull(RequiredNumberSchema())

export const RequiredIntegerSchema = () => RequiredNumberSchema().int()

export const NullableIntegerSchema = () =>
  nullableAndToNull(RequiredIntegerSchema())

export const RequiredBooleanSchema = () => z.boolean()

export const NullableBooleanSchema = () =>
  nullableAndToNull(RequiredBooleanSchema())

export const RequiredDateSchema = () =>
  z
    .custom<string | Date>(
      (value: any) => {
        if (isNullOrEmpty(value)) {
          return false
        }
        return dayjs(value).isValid()
      },
      { message: 'Invalid Date' },
    )
    .transform((value) => dayjs(value).toDate())

export const NullableDateSchema = () => nullableAndToNull(RequiredDateSchema())

export const RequiredEnumSchema = (enumType: any, ...excludedItems: string[]) =>
  z
    .nativeEnum(enumType)
    .refine((value) =>
      excludedItems?.length > 0 ? !excludedItems.includes(value) : true,
    )

export const NullableEnumSchema = (enumType: any, ...excludedItems: string[]) =>
  nullableAndToNull(RequiredEnumSchema(enumType, ...excludedItems))

export const AnySchema = () => z.any()

export const RequiredObjectSchema = <T extends ZodRawShape>(
  shape: T,
): ZodObject<T> => z.object(shape)

export const NullableObjectSchema = <T extends ZodRawShape>(shape: T) =>
  nullableAndToNull(RequiredObjectSchema(shape))

export const RequiredArraySchema = (schema: ZodSchema) =>
  z.array(schema).nonempty()

export const NullableArraySchema = (schema: ZodSchema) =>
  nullableAndToNull(z.array(schema))

export const DefaultArraySchema = (schema: ZodSchema) =>
  nullableAndToNull(z.array(schema)).transform((value) =>
    isNullOrEmpty(value) ? [] : value,
  )

export const PagingSchema = <T extends ZodRawShape>(shape?: T) =>
  RequiredObjectSchema({
    ...shape,
    keyword: NullableStringSchema(),
    page: z.number().int().min(1).default(1),
    limit: z.number().int().min(1).default(10),
    sort: nullableAndToNull(z.string().regex(/[+-]?[0-9a-zA-Z]*[,]?/)),
  })

export const RequiredObjectIdSchema = () =>
  z
    .custom<string | ObjectId>(
      (value: any) => {
        if (isNullOrEmpty(value) || !/^[0-9a-fA-F]{24}$/.test(`${value}`)) {
          return false
        }
        return ObjectId.isValid(value)
      },
      {
        message: 'Invalid ObjectId',
      },
    )
    .transform((value) => new ObjectId(value))

export const NullableObjectIdSchema = () =>
  nullableAndToNull(RequiredObjectIdSchema())

export const RequiredIdsSchema = () =>
  RequiredObjectSchema({
    ids: RequiredArraySchema(RequiredObjectIdSchema()),
  })

export const RequiredIdSchema = () =>
  RequiredObjectSchema({
    id: RequiredObjectIdSchema(),
  })

export type Infer<T extends ZodType<any, any, any>> = z.infer<T>
