import { ObjectId } from 'mongodb'
import { isArray, isObject } from '../helpers/util.helper'
import {
  DefaultArraySchema,
  NullableArraySchema,
  NullableBooleanSchema,
  NullableDateSchema,
  NullableEmailSchema,
  NullableEnumSchema,
  NullableIntegerSchema,
  NullableNumberSchema,
  NullableObjectIdSchema,
  NullableObjectSchema,
  NullableStringSchema,
  NullableUrlSchema,
  RequiredArraySchema,
  RequiredBooleanSchema,
  RequiredDateSchema,
  RequiredEmailSchema,
  RequiredEnumSchema,
  RequiredIntegerSchema,
  RequiredNumberSchema,
  RequiredObjectIdSchema,
  RequiredObjectSchema,
  RequiredStringSchema,
  RequiredUrlSchema,
} from './base.schema'

describe('Test RequiredStringSchema', () => {
  it(`should valid when 'foo'`, () => {
    const result = RequiredStringSchema().safeParse('foo')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('foo')
  })
  it(`should invalid when ''`, () => {
    const result = RequiredStringSchema().safeParse('')
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredStringSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredStringSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableStringSchema', () => {
  it(`should valid when 'foo'`, () => {
    const result = NullableStringSchema().safeParse('foo')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('foo')
  })
  it(`should valid when ''`, () => {
    const result = NullableStringSchema().safeParse('')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
  it(`should valid when null`, () => {
    const result = NullableStringSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
  it(`should valid when undefined`, () => {
    const result = NullableStringSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
})

describe('Test RequiredEmailSchema', () => {
  it(`should valid when 'foo@gg.cc'`, () => {
    const result = RequiredEmailSchema().safeParse('foo@gg.cc')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('foo@gg.cc')
  })
  it(`should invalid when 'foo'`, () => {
    const result = RequiredEmailSchema().safeParse('foo')
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredEmailSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredEmailSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableEmailSchema', () => {
  it(`should valid when 'foo@gg.cc'`, () => {
    const result = NullableEmailSchema().safeParse('foo@gg.cc')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('foo@gg.cc')
  })
  it(`should invalid when 'foo'`, () => {
    const result = NullableEmailSchema().safeParse('foo')
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableEmailSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
  it(`should valid when undefined`, () => {
    const result = NullableEmailSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
})

describe('Test RequiredUrlSchema', () => {
  it(`should valid when 'https://www.google.com'`, () => {
    const result = RequiredUrlSchema().safeParse('https://www.google.com')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('https://www.google.com')
  })
  it(`should invalid when 'foo'`, () => {
    const result = RequiredUrlSchema().safeParse('foo')
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredUrlSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredUrlSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableUrlSchema', () => {
  it(`should valid when 'https://www.google.com'`, () => {
    const result = NullableUrlSchema().safeParse('https://www.google.com')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('https://www.google.com')
  })
  it(`should invalid when 'foo'`, () => {
    const result = NullableUrlSchema().safeParse('foo')
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableUrlSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
  it(`should valid when undefined`, () => {
    const result = NullableUrlSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('')
  })
})

describe('Test RequiredNumberSchema', () => {
  it(`should valid when 123`, () => {
    const result = RequiredNumberSchema().safeParse(123)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(123)
  })
  it(`should invalid when '123'`, () => {
    const result = RequiredNumberSchema().safeParse('123')
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredNumberSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredNumberSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableNumberSchema', () => {
  it(`should valid when 123`, () => {
    const result = NullableNumberSchema().safeParse(123)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(123)
  })
  it(`should invalid when '123'`, () => {
    const result = NullableNumberSchema().safeParse('123')
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableNumberSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableNumberSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

describe('Test RequiredIntegerSchema', () => {
  it(`should valid when 123`, () => {
    const result = RequiredIntegerSchema().safeParse(123)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(123)
  })
  it(`should invalid when '123'`, () => {
    const result = RequiredIntegerSchema().safeParse('123')
    expect(result.success).toBe(false)
  })
  it(`should invalid when 123.4`, () => {
    const result = RequiredIntegerSchema().safeParse(123.4)
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredIntegerSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredIntegerSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableIntegerSchema', () => {
  it(`should valid when 123`, () => {
    const result = NullableIntegerSchema().safeParse(123)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(123)
  })
  it(`should invalid when '123'`, () => {
    const result = NullableIntegerSchema().safeParse('123')
    expect(result.success).toBe(false)
  })
  it(`should invalid when 123.4`, () => {
    const result = NullableIntegerSchema().safeParse(123.4)
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableIntegerSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableIntegerSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

describe('Test RequiredBooleanSchema', () => {
  it(`should valid when true`, () => {
    const result = RequiredBooleanSchema().safeParse(true)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(true)
  })
  it(`should valid when false`, () => {
    const result = RequiredBooleanSchema().safeParse(false)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(false)
  })
  it(`should invalid when 'true'`, () => {
    const result = RequiredBooleanSchema().safeParse('true')
    expect(result.success).toBe(false)
  })
  it(`should invalid when 'false'`, () => {
    const result = RequiredBooleanSchema().safeParse('false')
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredBooleanSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredBooleanSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableBooleanSchema', () => {
  it(`should valid when true`, () => {
    const result = NullableBooleanSchema().safeParse(true)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(true)
  })
  it(`should valid when false`, () => {
    const result = NullableBooleanSchema().safeParse(false)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(false)
  })
  it(`should invalid when 'true'`, () => {
    const result = NullableBooleanSchema().safeParse('true')
    expect(result.success).toBe(false)
  })
  it(`should invalid when 'false'`, () => {
    const result = NullableBooleanSchema().safeParse('false')
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableBooleanSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableBooleanSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

describe('Test RequiredDateSchema', () => {
  it(`should valid when '2023-01-01T00:00:00.000Z'`, () => {
    const result = RequiredDateSchema().safeParse('2023-01-01T00:00:00.000Z')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.getTime()).toBe(
      new Date('2023-01-01T08:00:00.000+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01T00:00:00.000+0800'`, () => {
    const result = RequiredDateSchema().safeParse(
      '2023-01-01T00:00:00.000+0800',
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.getTime()).toBe(
      new Date('2023-01-01T00:00:00.000+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01T11:22:33.444+0800'`, () => {
    const result = RequiredDateSchema().safeParse(
      '2023-01-01T11:22:33.444+0800',
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.getTime()).toBe(
      new Date('2023-01-01T11:22:33.444+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01'`, () => {
    const result = RequiredDateSchema().safeParse('2023-01-01')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.getTime()).toBe(
      new Date('2023-01-01T00:00:00.000+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01 11:22:33'`, () => {
    const result = RequiredDateSchema().safeParse('2023-01-01 11:22:33')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.getTime()).toBe(
      new Date('2023-01-01T11:22:33.000+0800').getTime(),
    )
  })
  it(`should invalid when null`, () => {
    const result = RequiredDateSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredDateSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableDateSchema', () => {
  it(`should valid when '2023-01-01T00:00:00.000Z'`, () => {
    const result = NullableDateSchema().safeParse('2023-01-01T00:00:00.000Z')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.getTime()).toBe(
      new Date('2023-01-01T08:00:00.000+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01T00:00:00.000+0800'`, () => {
    const result = NullableDateSchema().safeParse(
      '2023-01-01T00:00:00.000+0800',
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.getTime()).toBe(
      new Date('2023-01-01T00:00:00.000+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01T11:22:33.444+0800'`, () => {
    const result = NullableDateSchema().safeParse(
      '2023-01-01T11:22:33.444+0800',
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.getTime()).toBe(
      new Date('2023-01-01T11:22:33.444+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01'`, () => {
    const result = NullableDateSchema().safeParse('2023-01-01')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.getTime()).toBe(
      new Date('2023-01-01T00:00:00.000+0800').getTime(),
    )
  })
  it(`should valid when '2023-01-01 11:22:33'`, () => {
    const result = NullableDateSchema().safeParse('2023-01-01 11:22:33')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.getTime()).toBe(
      new Date('2023-01-01T11:22:33.000+0800').getTime(),
    )
  })
  it(`should valid when null`, () => {
    const result = NullableDateSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableDateSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

enum Gender {
  男 = 'Male',
  女 = 'Female',
}

describe('Test RequiredEnumSchema', () => {
  it(`should valid when Gender.男`, () => {
    const result = RequiredEnumSchema(Gender).safeParse(Gender.男)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Male')
    expect(result.data).toBe(Gender.男)
  })
  it(`should valid when 'Male'`, () => {
    const result = RequiredEnumSchema(Gender).safeParse('Male')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Male')
    expect(result.data).toBe(Gender.男)
  })
  it(`should valid when Gender.女`, () => {
    const result = RequiredEnumSchema(Gender).safeParse(Gender.女)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Female')
    expect(result.data).toBe(Gender.女)
  })
  it(`should valid when 'Female'`, () => {
    const result = RequiredEnumSchema(Gender).safeParse('Female')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Female')
    expect(result.data).toBe(Gender.女)
  })
  it(`should invalid when exclude 'Male'`, () => {
    const result = RequiredEnumSchema(Gender, Gender.男).safeParse(Gender.男)
    expect(result.success).toBe(false)
  })
  it(`should invalid when exclude ...[Gender.男]`, () => {
    const result = RequiredEnumSchema(
      Gender,
      ...[Gender.男, Gender.女],
    ).safeParse(Gender.男)
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredEnumSchema(Gender, Gender.女).safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredEnumSchema(Gender, Gender.女).safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableEnumSchema', () => {
  it(`should valid when Gender.男`, () => {
    const result = NullableEnumSchema(Gender).safeParse(Gender.男)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Male')
    expect(result.data).toBe(Gender.男)
  })
  it(`should valid when 'Male'`, () => {
    const result = NullableEnumSchema(Gender).safeParse('Male')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Male')
    expect(result.data).toBe(Gender.男)
  })
  it(`should valid when Gender.女`, () => {
    const result = NullableEnumSchema(Gender).safeParse(Gender.女)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Female')
    expect(result.data).toBe(Gender.女)
  })
  it(`should valid when 'Female'`, () => {
    const result = NullableEnumSchema(Gender).safeParse('Female')
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe('Female')
    expect(result.data).toBe(Gender.女)
  })
  it(`should invalid when exclude 'Male'`, () => {
    const result = NullableEnumSchema(Gender, Gender.男).safeParse(Gender.男)
    expect(result.success).toBe(false)
  })
  it(`should invalid when exclude ...[Gender.男]`, () => {
    const result = NullableEnumSchema(
      Gender,
      ...[Gender.男, Gender.女],
    ).safeParse(Gender.男)
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableEnumSchema(Gender, Gender.女).safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableEnumSchema(Gender, Gender.女).safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

class Foo {
  name!: string
}

describe('Test RequiredObjectSchema', () => {
  it(`should valid when {}`, () => {
    const result = RequiredObjectSchema({}).safeParse({})
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isObject(result.data)).toBe(true)
  })
  it(`should valid when new Object()`, () => {
    const result = RequiredObjectSchema({}).safeParse(new Object())
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isObject(result.data)).toBe(true)
  })
  it(`should valid when new Foo()`, () => {
    const result = RequiredObjectSchema({}).safeParse(new Foo())
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isObject(result.data)).toBe(true)
  })
  it(`should valid when { name: 'foo' }`, () => {
    const result = RequiredObjectSchema({
      name: RequiredStringSchema(),
    }).safeParse({ name: 'foo' })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.name).toBe('foo')
  })
  it(`should invalid when { name: null }`, () => {
    const result = RequiredObjectSchema({
      name: RequiredStringSchema(),
    }).safeParse({ name: null })
    expect(result.success).toBe(false)
  })
  it(`should valid when { name: 'foo' }`, () => {
    const result = RequiredObjectSchema({
      name: NullableStringSchema(),
    }).safeParse({ name: 'foo' })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.name).toBe('foo')
  })
  it(`should valid when { name: null }`, () => {
    const result = RequiredObjectSchema({
      name: NullableStringSchema(),
    }).safeParse({ name: null })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data.name).toBe('')
  })
  it(`should strip when { name: null, bar: 123 }`, () => {
    const result = RequiredObjectSchema({
      name: NullableStringSchema(),
    }).safeParse({ name: null, bar: 123 })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect((result.data as any).bar).toBe(undefined)
  })
  it(`should invalid when null`, () => {
    const result = RequiredObjectSchema({}).safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredObjectSchema({}).safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableObjectSchema', () => {
  it(`should valid when {}`, () => {
    const result = NullableObjectSchema({}).safeParse({})
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isObject(result.data)).toBe(true)
  })
  it(`should valid when new Object()`, () => {
    const result = NullableObjectSchema({}).safeParse(new Object())
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isObject(result.data)).toBe(true)
  })
  it(`should valid when new Foo()`, () => {
    const result = NullableObjectSchema({}).safeParse(new Foo())
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isObject(result.data)).toBe(true)
  })
  it(`should valid when { name: 'foo' }`, () => {
    const result = NullableObjectSchema({
      name: RequiredStringSchema(),
    }).safeParse({ name: 'foo' })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.name).toBe('foo')
  })
  it(`should invalid when { name: null }`, () => {
    const result = NullableObjectSchema({
      name: RequiredStringSchema(),
    }).safeParse({ name: null })
    expect(result.success).toBe(false)
  })
  it(`should valid when { name: 'foo' }`, () => {
    const result = NullableObjectSchema({
      name: NullableStringSchema(),
    }).safeParse({ name: 'foo' })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.name).toBe('foo')
  })
  it(`should valid when { name: null }`, () => {
    const result = NullableObjectSchema({
      name: NullableStringSchema(),
    }).safeParse({ name: null })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data?.name).toBe('')
  })
  it(`should strip when { name: null, bar: 123 }`, () => {
    const result = NullableObjectSchema({
      name: NullableStringSchema(),
    }).safeParse({ name: null, bar: 123 })
    expect(result.success).toBe(true)
    if (!result.success) return
    expect((<any>result.data).bar).toBe(undefined)
  })
  it(`should valid when null`, () => {
    const result = NullableObjectSchema({}).safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableObjectSchema({}).safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

describe('Test RequiredArraySchema', () => {
  it(`should valid when [1]`, () => {
    const result = RequiredArraySchema(RequiredIntegerSchema()).safeParse([1])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
    expect(result.data[0]).toBe(1)
  })
  it(`should valid when [{ name: 'foo' }]`, () => {
    const result = RequiredArraySchema(
      RequiredObjectSchema({
        name: RequiredStringSchema(),
      }),
    ).safeParse([{ name: 'foo' }])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data[0].name).toBe('foo')
  })
  it(`should invalid when []`, () => {
    const result = RequiredArraySchema(RequiredIntegerSchema()).safeParse([])
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredArraySchema(RequiredIntegerSchema()).safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredArraySchema(RequiredIntegerSchema()).safeParse(
      undefined,
    )
    expect(result.success).toBe(false)
  })
})

describe('Test NullableArraySchema', () => {
  it(`should valid when [1]`, () => {
    const result = NullableArraySchema(RequiredIntegerSchema()).safeParse([1])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
  })
  it(`should valid when []`, () => {
    const result = NullableArraySchema(RequiredIntegerSchema()).safeParse([])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
  })
  it(`should valid when [{ name: 'foo' }]`, () => {
    const result = NullableArraySchema(
      RequiredObjectSchema({
        name: RequiredStringSchema(),
      }),
    ).safeParse([{ name: 'foo' }])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect((<any>result.data)[0].name).toBe('foo')
  })
  it(`should valid when null`, () => {
    const result = NullableArraySchema(RequiredIntegerSchema()).safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableArraySchema(RequiredIntegerSchema()).safeParse(
      undefined,
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})

describe('Test DefaultArraySchema', () => {
  it(`should valid when [1]`, () => {
    const result = DefaultArraySchema(RequiredIntegerSchema()).safeParse([1])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
  })
  it(`should valid when []`, () => {
    const result = DefaultArraySchema(RequiredIntegerSchema()).safeParse([])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
  })
  it(`should valid when [{ name: 'foo' }]`, () => {
    const result = DefaultArraySchema(
      RequiredObjectSchema({
        name: RequiredStringSchema(),
      }),
    ).safeParse([{ name: 'foo' }])
    expect(result.success).toBe(true)
    if (!result.success) return
    expect((<any>result.data)[0].name).toBe('foo')
  })
  it(`should valid when null`, () => {
    const result = DefaultArraySchema(RequiredIntegerSchema()).safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
    expect(result.data?.length).toBe(0)
  })
  it(`should valid when undefined`, () => {
    const result = DefaultArraySchema(RequiredIntegerSchema()).safeParse(
      undefined,
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(isArray(result.data)).toBe(true)
    expect(result.data?.length).toBe(0)
  })
})

describe('Test RequiredObjectIdSchema', () => {
  it(`should valid when '64bbe6266e674728ee27271f'`, () => {
    const result = RequiredObjectIdSchema().safeParse(
      '64bbe6266e674728ee27271f',
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data instanceof ObjectId).toBe(true)
    expect(new ObjectId('64bbe6266e674728ee27271f').equals(result.data)).toBe(
      true,
    )
  })
  it(`should valid when new ObjectId('64bbe6266e674728ee27271f')`, () => {
    const result = RequiredObjectIdSchema().safeParse(
      new ObjectId('64bbe6266e674728ee27271f'),
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data instanceof ObjectId).toBe(true)
    expect(new ObjectId('64bbe6266e674728ee27271f').equals(result.data)).toBe(
      true,
    )
  })
  it(`should invalid when 'abc'`, () => {
    const result = RequiredObjectIdSchema().safeParse('abc')
    expect(result.success).toBe(false)
  })
  it(`should invalid when 123`, () => {
    const result = RequiredObjectIdSchema().safeParse(123)
    expect(result.success).toBe(false)
  })
  it(`should invalid when null`, () => {
    const result = RequiredObjectIdSchema().safeParse(null)
    expect(result.success).toBe(false)
  })
  it(`should invalid when undefined`, () => {
    const result = RequiredObjectIdSchema().safeParse(undefined)
    expect(result.success).toBe(false)
  })
})

describe('Test NullableObjectIdSchema', () => {
  it(`should valid when '64bbe6266e674728ee27271f'`, () => {
    const result = NullableObjectIdSchema().safeParse(
      '64bbe6266e674728ee27271f',
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data instanceof ObjectId).toBe(true)
    expect(new ObjectId('64bbe6266e674728ee27271f').equals(result.data!)).toBe(
      true,
    )
  })
  it(`should valid when new ObjectId('64bbe6266e674728ee27271f')`, () => {
    const result = NullableObjectIdSchema().safeParse(
      new ObjectId('64bbe6266e674728ee27271f'),
    )
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data instanceof ObjectId).toBe(true)
    expect(new ObjectId('64bbe6266e674728ee27271f').equals(result.data!)).toBe(
      true,
    )
  })
  it(`should invalid when 'abc'`, () => {
    const result = NullableObjectIdSchema().safeParse('abc')
    expect(result.success).toBe(false)
  })
  it(`should invalid when 123`, () => {
    const result = NullableObjectIdSchema().safeParse(123)
    expect(result.success).toBe(false)
  })
  it(`should valid when null`, () => {
    const result = NullableObjectIdSchema().safeParse(null)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
  it(`should valid when undefined`, () => {
    const result = NullableObjectIdSchema().safeParse(undefined)
    expect(result.success).toBe(true)
    if (!result.success) return
    expect(result.data).toBe(null)
  })
})
