import { Injectable } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { PagingQuery } from '../../models/dto/pagination'
import { DocName } from '../../models/entities/shared/doc-name'
import { Parent, ParentDto } from '../../models/entities/parent'
import { CommonQueryService } from '../../services/common-query.service'

@Injectable()
export class ParentsService {
  constructor(private commonQueryService: CommonQueryService) {}

  async queryParents(url: string, paging: PagingQuery) {
    const conditions = {}
    const parents = await this.commonQueryService.findWithPaging<Parent>(
      DocName.Parents,
      conditions,
      url,
      paging,
    )
    return parents
  }

  async getParent(_id: ObjectId) {
    const conditions = {
      _id,
    }
    const parent = await this.commonQueryService.findOneById<Parent>(
      DocName.Parents,
      conditions,
    )
    return parent
  }

  async createParent(parent: ParentDto) {
    const now = new Date()
    const { insertedId } = await this.commonQueryService.insertOne(
      now,
      DocName.Parents,
      parent,
    )
    return insertedId
  }

  async modifyParent(_id: ObjectId, parent: ParentDto) {
    const now = new Date()
    const conditions = {
      _id,
    }
    const update = {
      $set: {
        ...parent,
      },
    }
    const { matchedCount } = await this.commonQueryService.updateOneById(
      now,
      DocName.Parents,
      conditions,
      update,
    )
    return matchedCount
  }

  async removeParent(_id: ObjectId) {
    const conditions = {
      _id,
    }
    const { deletedCount } = await this.commonQueryService.deleteOneById(
      DocName.Parents,
      conditions,
    )
    return deletedCount
  }
}
