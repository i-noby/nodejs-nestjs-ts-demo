import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common'
import { ApiBody } from '@nestjs/swagger'
import { Request } from 'express'
import { ObjectId } from 'mongodb'
import { isNullOrUndefined } from '../../helpers/util.helper'
import { PagingQuery } from '../../models/dto/pagination'
import { Parent, ParentDto, ParentSchema } from '../../models/entities/parent'
import { ZodIdPipe } from '../../pipes/zod-id.pipe'
import { ZodPipe } from '../../pipes/zod.pipe'
import { PagingSchema } from '../../schema/base.schema'
import { ParentsService } from './parents.service'

@Controller('parents')
export class ParentsController {
  constructor(private parentsService: ParentsService) {}

  @Get()
  async queryParents(
    @Req() req: Request,
    @Query(new ZodPipe(PagingSchema())) query: PagingQuery,
  ) {
    const result = await this.parentsService.queryParents(req.url, query)
    return result
  }

  @Get(':id')
  async getParent(@Param('id', new ZodIdPipe()) id: ObjectId) {
    const parent = await this.parentsService.getParent(id)
    if (isNullOrUndefined(parent)) {
      throw new NotFoundException('Parent not found')
    }
    return { data: parent }
  }

  @Post()
  @ApiBody({ type: Parent })
  async createParent(@Body(new ZodPipe(ParentSchema())) body: ParentDto) {
    await this.parentsService.createParent(body)
  }

  @Put()
  @ApiBody({ type: Parent })
  async modifyParent(
    @Body('_id', new ZodIdPipe()) _id: ObjectId,
    @Body(new ZodPipe(ParentSchema())) body: ParentDto,
  ) {
    const matchedCount = await this.parentsService.modifyParent(_id, body)
    if (matchedCount === 0) {
      throw new NotFoundException('Parent not found')
    }
  }

  @Delete(':id')
  async removeParent(@Param('id', new ZodIdPipe()) id: ObjectId) {
    const deletedCount = await this.parentsService.removeParent(id)
    if (deletedCount === 0) {
      throw new NotFoundException('Parent not found')
    }
  }
}
