import { Module } from '@nestjs/common'
import { DatabaseModule } from '../../services/database.module'
import { ParentsController } from './parents.controller'
import { ParentsService } from './parents.service'

@Module({
  imports: [DatabaseModule],
  controllers: [ParentsController],
  providers: [ParentsService],
})
export class ParentsModule {}
