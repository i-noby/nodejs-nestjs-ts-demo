import { Controller, Get, Req } from '@nestjs/common'
import { Request } from 'express'
import requestIp from 'request-ip'

@Controller()
export class AppController {
  @Get()
  hi(@Req() req: Request) {
    const ip = requestIp.getClientIp(req)
    const result = {
      message: 'Hi, there!',
      ip,
    }
    return result
  }
}
