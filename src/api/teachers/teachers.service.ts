import { Injectable } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { PagingQuery } from '../../models/dto/pagination'
import { DocName } from '../../models/entities/shared/doc-name'
import { Teacher, TeacherDto } from '../../models/entities/teacher'
import { CommonQueryService } from '../../services/common-query.service'

@Injectable()
export class TeachersService {
  constructor(private commonQueryService: CommonQueryService) {}

  async queryTeachers(url: string, paging: PagingQuery) {
    const conditions = {}
    const teachers = await this.commonQueryService.findWithPaging<Teacher>(
      DocName.Teachers,
      conditions,
      url,
      paging,
    )
    return teachers
  }

  async getTeacher(_id: ObjectId) {
    const conditions = {
      _id,
    }
    const teacher = await this.commonQueryService.findOneById<Teacher>(
      DocName.Teachers,
      conditions,
    )
    return teacher
  }

  async createTeacher(teacher: TeacherDto) {
    const now = new Date()
    const { insertedId } = await this.commonQueryService.insertOne(
      now,
      DocName.Teachers,
      teacher,
    )
    return insertedId
  }

  async modifyTeacher(_id: ObjectId, teacher: TeacherDto) {
    const now = new Date()
    const conditions = {
      _id,
    }
    const update = {
      $set: {
        ...teacher,
      },
    }
    const { matchedCount } = await this.commonQueryService.updateOneById(
      now,
      DocName.Teachers,
      conditions,
      update,
    )
    return matchedCount
  }

  async removeTeacher(_id: ObjectId) {
    const conditions = {
      _id,
    }
    const { deletedCount } = await this.commonQueryService.deleteOneById(
      DocName.Teachers,
      conditions,
    )
    return deletedCount
  }
}
