import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common'
import { ApiBody } from '@nestjs/swagger'
import { Request } from 'express'
import { ObjectId } from 'mongodb'
import { isNullOrUndefined } from '../../helpers/util.helper'
import { PagingQuery } from '../../models/dto/pagination'
import {
  Teacher,
  TeacherDto,
  TeacherSchema,
} from '../../models/entities/teacher'
import { ZodIdPipe } from '../../pipes/zod-id.pipe'
import { ZodPipe } from '../../pipes/zod.pipe'
import { PagingSchema } from '../../schema/base.schema'
import { TeachersService } from './teachers.service'

@Controller('teachers')
export class TeachersController {
  constructor(private teachersService: TeachersService) {}

  @Get()
  async queryTeachers(
    @Req() req: Request,
    @Query(new ZodPipe(PagingSchema())) query: PagingQuery,
  ) {
    const result = await this.teachersService.queryTeachers(req.url, query)
    return result
  }

  @Get(':id')
  async getTeacher(@Param('id', new ZodIdPipe()) id: ObjectId) {
    const teacher = await this.teachersService.getTeacher(id)
    if (isNullOrUndefined(teacher)) {
      throw new NotFoundException('Teacher not found')
    }
    return { data: teacher }
  }

  @Post()
  @ApiBody({ type: Teacher })
  async createTeacher(@Body(new ZodPipe(TeacherSchema())) body: TeacherDto) {
    await this.teachersService.createTeacher(body)
  }

  @Put()
  @ApiBody({ type: Teacher })
  async modifyTeacher(
    @Body('_id', new ZodIdPipe()) _id: ObjectId,
    @Body(new ZodPipe(TeacherSchema())) body: TeacherDto,
  ) {
    const matchedCount = await this.teachersService.modifyTeacher(_id, body)
    if (matchedCount === 0) {
      throw new NotFoundException('Teacher not found')
    }
  }

  @Delete(':id')
  async removeTeacher(@Param('id', new ZodIdPipe()) id: ObjectId) {
    const deletedCount = await this.teachersService.removeTeacher(id)
    if (deletedCount === 0) {
      throw new NotFoundException('Teacher not found')
    }
  }
}
