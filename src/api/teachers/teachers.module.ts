import { Module } from '@nestjs/common'
import { DatabaseModule } from '../../services/database.module'
import { TeachersController } from './teachers.controller'
import { TeachersService } from './teachers.service'

@Module({
  imports: [DatabaseModule],
  controllers: [TeachersController],
  providers: [TeachersService],
})
export class TeachersModule {}
