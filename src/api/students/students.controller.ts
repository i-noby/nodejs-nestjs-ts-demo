import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common'
import { ApiBody } from '@nestjs/swagger'
import { Request } from 'express'
import { ObjectId } from 'mongodb'
import { isNullOrUndefined } from '../../helpers/util.helper'
import { PagingQuery } from '../../models/dto/pagination'
import {
  Student,
  StudentDto,
  StudentSchema,
} from '../../models/entities/student'
import { ZodIdPipe } from '../../pipes/zod-id.pipe'
import { ZodPipe } from '../../pipes/zod.pipe'
import { PagingSchema } from '../../schema/base.schema'
import { StudentsService } from './students.service'

@Controller('students')
export class StudentsController {
  constructor(private studentsService: StudentsService) {}

  @Get()
  async queryStudents(
    @Req() req: Request,
    @Query(new ZodPipe(PagingSchema())) query: PagingQuery,
  ) {
    const result = await this.studentsService.queryStudents(req.url, query)
    return result
  }

  @Get(':id')
  async getStudent(@Param('id', new ZodIdPipe()) id: ObjectId) {
    const student = await this.studentsService.getStudent(id)
    if (isNullOrUndefined(student)) {
      throw new NotFoundException('Student not found')
    }
    return { data: student }
  }

  @Post()
  @ApiBody({ type: Student })
  async createStudent(@Body(new ZodPipe(StudentSchema())) body: StudentDto) {
    await this.studentsService.createStudent(body)
  }

  @Put()
  @ApiBody({ type: Student })
  async modifyStudent(
    @Body('_id', new ZodIdPipe()) _id: ObjectId,
    @Body(new ZodPipe(StudentSchema())) body: StudentDto,
  ) {
    const matchedCount = await this.studentsService.modifyStudent(_id, body)
    if (matchedCount === 0) {
      throw new NotFoundException('Student not found')
    }
  }

  @Delete(':id')
  async removeStudent(@Param('id', new ZodIdPipe()) id: ObjectId) {
    const deletedCount = await this.studentsService.removeStudent(id)
    if (deletedCount === 0) {
      throw new NotFoundException('Student not found')
    }
  }
}
