import { Injectable } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { PagingQuery } from '../../models/dto/pagination'
import { DocName } from '../../models/entities/shared/doc-name'
import { Student, StudentDto } from '../../models/entities/student'
import { CommonQueryService } from '../../services/common-query.service'

@Injectable()
export class StudentsService {
  constructor(private commonQueryService: CommonQueryService) {}

  async queryStudents(url: string, paging: PagingQuery) {
    const conditions = {}
    const students = await this.commonQueryService.findWithPaging<Student>(
      DocName.Students,
      conditions,
      url,
      paging,
    )
    return students
  }

  async getStudent(_id: ObjectId) {
    const conditions = {
      _id,
    }
    const student = await this.commonQueryService.findOneById<Student>(
      DocName.Students,
      conditions,
    )
    return student
  }

  async createStudent(student: StudentDto) {
    const now = new Date()
    const { insertedId } = await this.commonQueryService.insertOne(
      now,
      DocName.Students,
      student,
    )
    return insertedId
  }

  async modifyStudent(_id: ObjectId, student: StudentDto) {
    const now = new Date()
    const conditions = {
      _id,
    }
    const update = {
      $set: {
        ...student,
      },
    }
    const { matchedCount } = await this.commonQueryService.updateOneById(
      now,
      DocName.Students,
      conditions,
      update,
    )
    return matchedCount
  }

  async removeStudent(_id: ObjectId) {
    const conditions = {
      _id,
    }
    const { deletedCount } = await this.commonQueryService.deleteOneById(
      DocName.Students,
      conditions,
    )
    return deletedCount
  }
}
