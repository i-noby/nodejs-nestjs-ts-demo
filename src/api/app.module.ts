import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { AppController } from './app.controller'
import { StudentsModule } from './students/students.module'
import { TeachersModule } from './teachers/teachers.module'
import { ParentsModule } from './parents/parents.module'

@Module({
  imports: [
    ParentsModule,
    TeachersModule,
    StudentsModule,
    ConfigModule.forRoot(),
  ],
  controllers: [AppController],
})
export class AppModule {}
