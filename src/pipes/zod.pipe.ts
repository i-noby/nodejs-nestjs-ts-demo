import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common'
import { ZodSchema } from 'zod'

@Injectable()
export class ZodPipe implements PipeTransform {
  constructor(private schema: ZodSchema) {}

  transform(orgValue: any) {
    const result = this.schema.safeParse(orgValue)
    if (result.success === false) {
      throw new BadRequestException({
        statusCode: 400,
        message: 'Bad Request',
        error: result.error.format(),
      })
    }
    return result.data
  }
}
