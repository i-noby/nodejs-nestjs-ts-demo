import { Injectable } from '@nestjs/common'
import { RequiredObjectIdSchema } from '../schema/base.schema'
import { ZodPipe } from './zod.pipe'

@Injectable()
export class ZodIdPipe extends ZodPipe {
  constructor() {
    super(RequiredObjectIdSchema())
  }
}
