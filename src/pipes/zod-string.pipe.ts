import { Injectable } from '@nestjs/common'
import { RequiredStringSchema } from '../schema/base.schema'
import { ZodPipe } from './zod.pipe'

@Injectable()
export class ZodStringPipe extends ZodPipe {
  constructor() {
    super(RequiredStringSchema())
  }
}
