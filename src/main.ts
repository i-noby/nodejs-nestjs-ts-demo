import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { writeFileSync } from 'fs'
import { AppModule } from './api/app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const config = new DocumentBuilder()
    .setTitle('API 完全攻略')
    .setDescription(
      `For the brave souls who get this far: You are the chosen ones,\n
the valiant knights of programming who toil away, without rest,\n
fixing our most awful code. To you, true saviors, kings of men,\n
I say this: never gonna give you up, never gonna let you down,\n
never gonna run around and desert you. Never gonna make you cry,\n
never gonna say goodbye. Never gonna tell a lie and hurt you.`,
    )
    .setVersion('1.0')
    .addServer('http://localhost:3000')
    .addSecurity('bearer', {
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT',
    })
    .build()
  const document = SwaggerModule.createDocument(app, config)
  writeFileSync('doc.json', JSON.stringify(document, null, 2))
  SwaggerModule.setup('doc', app, document)
  await app.listen(3000)
}
bootstrap()
