const crudGenerator = require('./generators/crud/crud.generator')
const entityGenerator = require('./generators/entity/entity.generator')
const { execSync } = require('child_process')

module.exports = function (plop) {
  plop.__force__ = true
  plop.setActionType('format', () => {
    try {
      execSync('npm run format')
      return 'format succeeded'
    } catch (error) {
      return 'format failed'
    }
  })
  crudGenerator(plop)
  entityGenerator(plop)
}
