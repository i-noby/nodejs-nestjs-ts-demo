const pluralize = require('pluralize')
const fs = require('fs')
const path = require('path')
const { pascalCase } = require('change-case')

module.exports = function (plop) {
  plop.setHelper('ternary', (value, a, b) => (value ? a : b))
  plop.setHelper('eq', (a, b) => a === b)
  plop.setHelper('ne', (a, b) => a !== b)
  plop.setHelper('getSchemaImport', (fields) =>
    [
      ...new Set(
        fields.map((x) =>
          x.name === '_id'
            ? null
            : `${x.required ? 'Required' : 'Nullable'}${x.type}Schema`,
        ),
      ),
    ]
      .filter((x) => x)
      .join(','),
  )
  plop.setGenerator('entity', {
    description: '實體',
    prompts: [],
    actions() {
      const file = fs.readFileSync(
        path.join(__dirname, '../../entity.json'),
        'utf8',
      )
      const entities = JSON.parse(file)
      const actions = entities.flatMap((entity) => {
        const data = {
          singular: entity.name,
          plural: pluralize(entity.name),
          entity,
        }
        return [
          {
            type: 'add',
            path: 'src/models/entities/{{kebabCase singular}}.ts',
            templateFile: 'generators/entity/entity.hbs',
            force: plop.__force__,
            data,
          },
          {
            type: 'modify',
            path: 'src/models/entities/shared/doc-name.ts',
            transform(contents, data) {
              const pascalCaseName = pascalCase(data.plural)
              if (contents.includes(pascalCaseName)) {
                return contents
              }
              return contents.replace(
                /export enum DocName \{/g,
                `export enum DocName {
  ${pascalCaseName} = '${pascalCaseName}',`,
              )
            },
            data,
          },
        ]
      })
      actions.push({
        type: 'format',
      })
      return actions
    },
  })
}
