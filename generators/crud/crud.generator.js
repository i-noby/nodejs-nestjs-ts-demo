const pluralize = require('pluralize')
const { pascalCase, camelCase } = require('change-case')

module.exports = function (plop) {
  plop.setGenerator('crud', {
    description: '基本五式',
    prompts: [
      {
        type: 'input',
        name: 'names',
        message: '蝦米名? (單數名詞)',
      },
    ],
    actions(data) {
      const names = data.names
        .split(/\s+/)
        .map((name) => name.trim())
        .filter((x) => x)
      const actions = names.flatMap((name) => {
        const data = {
          singular: name,
          plural: pluralize(name),
        }
        return [
          {
            type: 'addMany',
            destination: 'src/api/{{plural}}',
            base: 'generators/crud/api',
            templateFiles: 'generators/crud/api/*.hbs',
            force: plop.__force__,
            data,
          },
          {
            type: 'modify',
            path: 'src/api/app.module.ts',
            transform(contents, data) {
              const pascalCaseName = pascalCase(data.plural)
              const camelCaseName = camelCase(data.plural)
              const name = `${pascalCase(data.plural)}Module`
              if (contents.includes(name)) {
                return contents
              }
              return contents.replace(
                /\n@Module\(\{\n  imports: \[/g,
                `import { ${pascalCaseName}Module } from './${camelCaseName}/${camelCaseName}.module'

@Module({
  imports: [
    ${pascalCaseName}Module, `,
              )
            },
            data,
          },
        ]
      })
      actions.push({
        type: 'format',
      })
      return actions
    },
  })
}

/*
        {
          type: 'modify',
          path: 'src/api/app.module.ts',
          pattern: /\n@Module\(\{\n  imports: \[/g,
          template: `import { {{pascalCase plural}}Module } from './{{camelCase plural}}/{{camelCase plural}}.module'

@Module({
  imports: [
    {{pascalCase plural}}Module, `,
          skip: function (...args) {
            console.log(args)
          },
        },
        {
          type: 'modify',
          path: 'src/models/entities/shared/doc-name.ts',
          pattern: /export enum DocName \{/g,
          template: `export enum DocName {
  {{pascalCase plural}} = '{{pascalCase plural}}',`,
        },
*/
